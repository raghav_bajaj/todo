import {Entity, model, property, belongsTo} from '@loopback/repository';
import {Todo} from './todo.model';

@model()
export class Task extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: false,
    required: true,
  })
  todo_id: number;

  @property({
    type: 'date',
    required: true,
  })
  date: string;

  @property({
    type: 'string',
    required: true,
  })
  complexity: string;

  @property({
    type: 'string',
  })
  assigned?: string;

  @belongsTo(() => Todo, {name: 'redos'})
  todoId: number;

  constructor(data?: Partial<Task>) {
    super(data);
  }
}

export interface TaskRelations {
  // describe navigational properties here
}

export type TaskWithRelations = Task & TaskRelations;
