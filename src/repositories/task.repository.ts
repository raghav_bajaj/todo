import {inject, Getter} from '@loopback/core';
import {DefaultCrudRepository, repository, BelongsToAccessor} from '@loopback/repository';
import {TodoDataSource} from '../datasources';
import {Task, TaskRelations, Todo} from '../models';
import {TodoRepository} from './todo.repository';

export class TaskRepository extends DefaultCrudRepository<
  Task,
  typeof Task.prototype.todo_id,
  TaskRelations
> {

  public readonly redos: BelongsToAccessor<Todo, typeof Task.prototype.todo_id>;

  constructor(
    @inject('datasources.todo') dataSource: TodoDataSource, @repository.getter('TodoRepository') protected todoRepositoryGetter: Getter<TodoRepository>,
  ) {
    super(Task, dataSource);
    this.redos = this.createBelongsToAccessorFor('redos', todoRepositoryGetter,);
    this.registerInclusionResolver('redos', this.redos.inclusionResolver);
  }
}
