import {
  repository,
} from '@loopback/repository';
import {
  param,
  get,
  getModelSchemaRef,
} from '@loopback/rest';
import {
  Task,
  Todo,
} from '../models';
import {TaskRepository} from '../repositories';

export class TaskTodoController {
  constructor(
    @repository(TaskRepository)
    public taskRepository: TaskRepository,
  ) { }

  @get('/tasks/{id}/todo', {
    responses: {
      '200': {
        description: 'Todo belonging to Task',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Todo)},
          },
        },
      },
    },
  })
  async getTodo(
    @param.path.number('id') id: typeof Task.prototype.todo_id,
  ): Promise<Todo> {
    return this.taskRepository.redos(id);
  }
}
