import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody,
} from '@loopback/rest';
import {
  Todo,
  Task,
} from '../models';
import {TodoRepository} from '../repositories';

export class TodoTaskController {
  constructor(
    @repository(TodoRepository) protected todoRepository: TodoRepository,
  ) { }

  @get('/todos/{id}/tasks', {
    responses: {
      '200': {
        description: 'Array of Todo has many Task',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Task)},
          },
        },
      },
    },
  })
  async find(
    @param.path.number('id') id: number,
    @param.query.object('filter') filter?: Filter<Task>,
  ): Promise<Task[]> {
    return this.todoRepository.tasks(id).find(filter);
  }

  @post('/todos/{id}/tasks', {
    responses: {
      '200': {
        description: 'Todo model instance',
        content: {'application/json': {schema: getModelSchemaRef(Task)}},
      },
    },
  })
  async create(
    @param.path.number('id') id: typeof Todo.prototype.id,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Task, {
            title: 'NewTaskInTodo',
            exclude: ['todo_id'],
            optional: ['todoId']
          }),
        },
      },
    }) task: Omit<Task, 'todo_id'>,
  ): Promise<Task> {
    return this.todoRepository.tasks(id).create(task);
  }

  @patch('/todos/{id}/tasks', {
    responses: {
      '200': {
        description: 'Todo.Task PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async patch(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Task, {partial: true}),
        },
      },
    })
    task: Partial<Task>,
    @param.query.object('where', getWhereSchemaFor(Task)) where?: Where<Task>,
  ): Promise<Count> {
    return this.todoRepository.tasks(id).patch(task, where);
  }

  @del('/todos/{id}/tasks', {
    responses: {
      '200': {
        description: 'Todo.Task DELETE success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async delete(
    @param.path.number('id') id: number,
    @param.query.object('where', getWhereSchemaFor(Task)) where?: Where<Task>,
  ): Promise<Count> {
    return this.todoRepository.tasks(id).delete(where);
  }
}
